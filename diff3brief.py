#!/usr/bin/env python3

import argparse
import os

PROGRAM_NAME = 'diff3brief'
PROGRAM_VERSION = '0.1.0'

def check_dir(dir):
  if not os.path.exists(dir):
    parser.error(dir + ': No such directory')
  if not os.path.isdir(dir):
    parser.error(dir + ': Is not a directory')

def get_contents(filename):
  f = open(filename, 'rb')
  result = f.read()
  f.close()
  return result

def is_same(file1, file2):
  file1_exists = os.path.exists(file1)
  file2_exists = os.path.exists(file2)
  if file1_exists and not file2_exists:
    return False
  if not file1_exists and file2_exists:
    return False
  if not file1_exists and not file2_exists:
    return True
  file1_is_directory = os.path.isdir(file1)
  file2_is_directory = os.path.isdir(file2)
  if file1_is_directory and not file2_is_directory:
    return False
  if not file1_is_directory and file2_is_directory:
    return False
  if file1_is_directory and file2_is_directory:
    return True
  file1_contents = get_contents(file1)
  file2_contents = get_contents(file2)
  return file1_contents == file2_contents

def process_directory_recursively(conflicts, root_directory, dir1, dir2, dir3, subdirectory = None):
  if subdirectory is None:
    full_path = root_directory
  else:
    full_path = root_directory + '/' + subdirectory
  entries = os.listdir(full_path)
  for entry in entries:
    if subdirectory is None:
      path = entry
    else:
      path = subdirectory + '/' + entry

    # sanity check
    assert root_directory + '/' + path == full_path + '/' + entry

    # compare all three
    file1 = dir1 + '/' + path
    file2 = dir2 + '/' + path
    file3 = dir3 + '/' + path
    if path not in conflicts and not is_same(file1, file2) and not is_same(file1, file3) and not is_same(file2, file3):
      conflicts.add(path)

    if os.path.isdir(root_directory + '/' + path):
      process_directory_recursively(conflicts, root_directory, dir1, dir2, dir3, path)

def find_conflicts(dir1, dir2, dir3):
  dir1 = dir1.rstrip('/')
  dir2 = dir2.rstrip('/')
  dir3 = dir3.rstrip('/')
  conflicts = set()
  process_directory_recursively(conflicts, dir1, dir1, dir2, dir3)
  process_directory_recursively(conflicts, dir2, dir1, dir2, dir3)
  return sorted(conflicts)

if __name__ == '__main__':
  parser = argparse.ArgumentParser(description = 'Given three directories, list the files that differ in ALL of them.')
  parser.add_argument('--version', action = 'version', version = '%(prog)s ' + PROGRAM_VERSION)
  parser.add_argument('dir1', help = 'the first directory')
  parser.add_argument('dir2', help = 'the second directory')
  parser.add_argument('dir3', help = 'the third directory')
  args = parser.parse_args()

  dir1 = args.dir1
  dir2 = args.dir2
  dir3 = args.dir3

  check_dir(dir1)
  check_dir(dir2)
  check_dir(dir3)

  conflicts = find_conflicts(dir1, dir2, dir3)
  for conflict in conflicts:
    print(conflict)
