import os
import shutil
import unittest

import diff3brief

class TestFindConflicts(unittest.TestCase):
  def setUp(self):
    dirs = self.get_dirs()
    for d in dirs:
      os.mkdir(d + '/subdir')
      entries = os.scandir(d)
      for entry in entries:
        if entry.is_file():
          shutil.copyfile(entry.path, d + '/subdir/' + entry.name)

  def tearDown(self):
    dirs = self.get_dirs()
    for d in dirs:
      shutil.rmtree(d + '/subdir')

  def get_dirs(self):
    dir1 = 'tests/version-1'
    dir2 = 'tests/version-2'
    dir3 = 'tests/vendor-version'
    return (dir1, dir2, dir3)

  def test_find_conflicts(self):
    find_conflicts = diff3brief.find_conflicts

    (dir1, dir2, dir3) = self.get_dirs()

    expected = [
      'different-changes-in-version-2-and-vendor-version.txt',
      'different-files-added-in-version-2-and-vendor-version.txt',
      'subdir/different-changes-in-version-2-and-vendor-version.txt',
      'subdir/different-files-added-in-version-2-and-vendor-version.txt',
    ]

    conflicts = find_conflicts(dir1, dir2, dir3)
    self.assertEqual(expected, conflicts)

    conflicts = find_conflicts(dir1, dir3, dir2)
    self.assertEqual(expected, conflicts)

    conflicts = find_conflicts(dir2, dir1, dir3)
    self.assertEqual(expected, conflicts)

    conflicts = find_conflicts(dir2, dir3, dir1)
    self.assertEqual(expected, conflicts)

    conflicts = find_conflicts(dir3, dir1, dir2)
    self.assertEqual(expected, conflicts)

    conflicts = find_conflicts(dir3, dir2, dir1)
    self.assertEqual(expected, conflicts)

if __name__ == '__main__':
  unittest.main()
