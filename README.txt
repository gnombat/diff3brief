This is a very simple program (less than 100 lines of Python code) which takes
three directories as input and lists the files that are different and unique in
all three of them.

This program requires Python 3.

This program is free software. See the file "LICENSE.txt" for license terms.
